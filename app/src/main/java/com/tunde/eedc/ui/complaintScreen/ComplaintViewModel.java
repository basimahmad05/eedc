package com.tunde.eedc.ui.complaintScreen;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.tunde.eedc.services.DataModels.complaintModels.AddComplaintApiResponse;
import com.tunde.eedc.services.DataModels.complaintModels.ComplaintDataModel;
import com.tunde.eedc.services.DataModels.customerModels.UserDataApiResponse;
import com.tunde.eedc.services.Repositories.AppRepository;
import com.tunde.eedc.utils.Validation;

public class ComplaintViewModel extends ViewModel {
    public MutableLiveData<String> mComplaintName;
    public MutableLiveData<String> complaintType;
    public MutableLiveData<String> complainantNo;
    public MutableLiveData<String> complaint;
    public MutableLiveData<String> accountNo;
    public MutableLiveData<String> address;
    public MutableLiveData<String> district;
    private LiveData<AddComplaintApiResponse> complaintDataObservable = null;
    private LiveData<UserDataApiResponse> userDataObservable = null;

    public ComplaintViewModel() {
        mComplaintName = new MutableLiveData<>();
        complaintType = new MutableLiveData<>();
        complainantNo = new MutableLiveData<>();
        complaint = new MutableLiveData<>();
        accountNo = new MutableLiveData<>();
        address = new MutableLiveData<>();
        district = new MutableLiveData<>();
    }

    public void userDetailViewModelCall(Context context, String accountNumber){
        if (Validation.isConnected(context)) {
            userDataObservable = AppRepository.getInstance().getUserDataCall(context, accountNumber);
        }
    }

    public LiveData<UserDataApiResponse> getUserDetail(){
        return userDataObservable;
    }

    public void addComplaintViewModelCall(Context context, ComplaintDataModel dataObj){
        if (Validation.isConnected(context)) {
            complaintDataObservable = AppRepository.getInstance().addComplaintCall(context, dataObj);
        }
    }

    public LiveData<AddComplaintApiResponse> getComplaintApiResp(){
        return complaintDataObservable;
    }

    public MutableLiveData<String> getmComplaintName() {
        return mComplaintName;
    }

    public void setmComplaintName(String data) {
        mComplaintName.setValue(data);
    }

    public MutableLiveData<String> getComplaintType() {
        return complaintType;
    }

    public void setComplaintType(String data) {
        complaintType.setValue(data);
    }

    public MutableLiveData<String> getComplainantNo() {
        return complainantNo;
    }

    public void setComplainantNo(String data) {
        complainantNo.setValue(data);
    }

    public MutableLiveData<String> getComplaint() {
        return complaint;
    }

    public void setComplaint(String data) {
        complaint.setValue(data);
    }

    public MutableLiveData<String> getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String data) {
        accountNo.setValue(data);
    }

    public MutableLiveData<String> getAddress() {
        return address;
    }

    public void setAddress(String data) {
        address.setValue(data);
    }

    public MutableLiveData<String> getDistrict() {
        return district;
    }

    public void setDistrict(String data) {
        district.setValue(data);
    }
}