package com.tunde.eedc.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.tunde.eedc.ui.MainActivity;
import com.tunde.eedc.R;
import com.tunde.eedc.ui.webView.WebViewActivity;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        setHasOptionsMenu(false);
        MainActivity.setHomeIcon(false);

        ConstraintLayout paymentHistory = root.findViewById(R.id.button_payment_history);
        paymentHistory.setOnClickListener(view -> MainActivity.navController.navigate(R.id.nav_payment_history));

        ConstraintLayout logComplaint = root.findViewById(R.id.button_log_complaints);
        logComplaint.setOnClickListener(view -> MainActivity.navController.navigate(R.id.nav_log_complaint));

        ConstraintLayout notifications = root.findViewById(R.id.button_notification);
        notifications.setOnClickListener(view ->
                MainActivity.navController.navigate(R.id.nav_notification));

        ConstraintLayout buyEnergy = root.findViewById(R.id.button_buy_energy);
        buyEnergy.setOnClickListener(view -> {
            Intent mainIntent = new Intent(getContext(), WebViewActivity.class);
            getActivity().startActivity(mainIntent);
        });

        return root;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item=menu.findItem(R.id.action_home);
        if(item!=null)
            item.setVisible(false);
    }
}