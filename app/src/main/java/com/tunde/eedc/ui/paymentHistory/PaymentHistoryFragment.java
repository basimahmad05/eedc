package com.tunde.eedc.ui.paymentHistory;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.tunde.eedc.ui.MainActivity;
import com.tunde.eedc.R;

import java.util.ArrayList;
import java.util.List;

public class PaymentHistoryFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private PaymentHistoryViewModel paymentHistoryViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        paymentHistoryViewModel =
                ViewModelProviders.of(this).get(PaymentHistoryViewModel.class);
        View root = inflater.inflate(R.layout.fragment_payment_history, container, false);
        MainActivity.setHomeIcon(true);

        Spinner spinner = (Spinner) root.findViewById(R.id.spinner_month);
        spinner.setOnItemSelectedListener(this);

        List<String> categories = new ArrayList<String>();
        categories.add("This Month");
        categories.add("Last Month");
        categories.add("This Year");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);

        ConstraintLayout confirmTransaction = root.findViewById(R.id.layout_confirm_transaction);
        confirmTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                MainActivity.navController.navigate(R.id.nav_transaction_result);
            }
        });

        ConstraintLayout declineTransaction = root.findViewById(R.id.layout_confirm_transaction_two);
        declineTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                MainActivity.navController.navigate(R.id.nav_transaction_result);
            }
        });

        return root;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        // On selecting a spinner item
        String item = adapterView.getItemAtPosition(i).toString();

        // Showing selected spinner item
        Toast.makeText(adapterView.getContext(), item+ " Selected", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}