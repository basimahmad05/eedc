package com.tunde.eedc.ui.quickGuide;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class QuickGuideViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public QuickGuideViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is quick guide fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}