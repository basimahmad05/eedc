package com.tunde.eedc.ui.profile;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ProfileViewModel extends ViewModel {
    public MutableLiveData<String> mName;
    public MutableLiveData<String> mEmail;
    public MutableLiveData<String> mAccountNumber;
    public MutableLiveData<String> mMeterNumber;
    public MutableLiveData<String> mAddress;
    public MutableLiveData<String> mDistrict;
    public MutableLiveData<String> mTerrif;

    public ProfileViewModel() {
        mAccountNumber = new MutableLiveData<>();
        mAddress = new MutableLiveData<>();
        mName = new MutableLiveData<>();
        mEmail = new MutableLiveData<>();
        mMeterNumber = new MutableLiveData<>();
        mDistrict = new MutableLiveData<>();
        mTerrif = new MutableLiveData<>();
    }

    public MutableLiveData<String> getmName() {
        return mName;
    }

    public void setmName(String data) {
        mName.setValue(data);
    }

    public MutableLiveData<String> getmEmail() {
        return mEmail;
    }

    public void setmEmail(String data) {
        mEmail.setValue(data);
    }

    public MutableLiveData<String> getmAccountNumber() {
        return mAccountNumber;
    }

    public void setmAccountNumber(String data) {
        mAccountNumber.setValue(data);
    }

    public MutableLiveData<String> getmMeterNumber() {
        return mMeterNumber;
    }

    public void setmMeterNumber(String data) {
        mMeterNumber.setValue(data);
    }

    public MutableLiveData<String> getmAddress() {
        return mAddress;
    }

    public void setmAddress(String data) {
        mAddress.setValue(data);
    }

    public MutableLiveData<String> getmDistrict() {
        return mDistrict;
    }

    public void setmDistrict(String data) {
        mDistrict.setValue(data);
    }

    public MutableLiveData<String> getmTerrif() {
        return mTerrif;
    }

    public void setmTerrif(String data) {
        mTerrif.setValue(data);
    }
}