package com.tunde.eedc.ui.customerDetail;

import android.app.AlertDialog;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;
import com.tunde.eedc.R;
import com.tunde.eedc.services.DataModels.customerModels.Customer;
import com.tunde.eedc.ui.MainActivity;
import com.tunde.eedc.utils.PrefManager;

import java.util.Objects;

public class CustomerDetailFragment extends Fragment {
    private View root;
    private CustomerDetailViewModel customerDetailViewModel;
    private EditText etCurrentBill, etNumber, etAddress;
    private TextView tvBillDate, tvArrearBalance, tvBillAmount, tvVat;
    private Button btnSearch, btnNext;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        customerDetailViewModel =
                ViewModelProviders.of(this).get(CustomerDetailViewModel.class);
        root = inflater.inflate(R.layout.fragment_customer_detail, container, false);


        initViews();

        return root;
    }

    private void initViews() {
        etCurrentBill = root.findViewById(R.id.et_your_current_bill);
        etNumber = root.findViewById(R.id.et_number);
        etAddress = root.findViewById(R.id.et_address);
        btnSearch = root.findViewById(R.id.button_search);
        btnNext= root.findViewById(R.id.button_next);
        tvBillDate = root.findViewById(R.id.text_bill_date);
        tvArrearBalance = root.findViewById(R.id.text_arrear_balance);
        tvBillAmount = root.findViewById(R.id.text_billed_amount);
        tvVat = root.findViewById(R.id.text_vat);

        etCurrentBill.setKeyListener(null);
        setClickListeners();
        setObservers();
        if(PrefManager.getUserData(getContext()) != null){
            setDataFields(PrefManager.getUserData(getContext()));
        }
    }

    private void setObservers() {
        customerDetailViewModel.getAccountNumber().observe(this, s -> etNumber.setText(s));

        customerDetailViewModel.getAddress().observe(this, s -> etAddress.setText(s));

        customerDetailViewModel.getBilledDate().observe(this, s -> tvBillDate.setText(s));

        customerDetailViewModel.getArrearBalance().observe(this, s -> tvArrearBalance.setText(s));

        customerDetailViewModel.getBilledAmount().observe(this, s -> tvBillAmount.setText(s));

        customerDetailViewModel.getVat().observe(this, s -> tvVat.setText(s));
    }

    private void setDataFields(Customer userData) {
        customerDetailViewModel.setAccountNumber(userData.getAccountNumber());
        customerDetailViewModel.setAddress(userData.getFirstName()+" "+userData.getLastName()+"\n\n "+userData.getAddress());
        customerDetailViewModel.setBilledDate(userData.getBilledDate());
        customerDetailViewModel.setmArrearsBalance("$ "+userData.getArrearsBalance());
        customerDetailViewModel.setBilledAmount("$ "+userData.getBilledAmount());
        customerDetailViewModel.setVat("$ "+userData.getVat());
    }

    private void setClickListeners() {
        btnSearch.setOnClickListener(v -> {
            if (etNumber.getText().toString().equals("")) {
                showSnack(Objects.requireNonNull(getContext()).getResources().getString(R.string.enter_account_number));
            } else {
                fetchUserDetail(etNumber.getText().toString());
            }
        });

        btnNext.setOnClickListener(view -> MainActivity.navController.navigate(R.id.nav_payment_detail));
    }

    private void showSnack(String message) {
        Snackbar snackbar = Snackbar
                .make(root, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }


    private void fetchUserDetail(String accountNumber) {
        customerDetailViewModel.userDetailViewModelCall(getContext(), accountNumber);
        observableUser();
    }

    private void observableUser() {
        customerDetailViewModel.getUserDetail().observe(this, resp -> {
            if(resp != null) {
                if (resp.getResponseCode() == 200) {
                    PrefManager.saveUserData(resp.getCustomer(), getContext());
                    setDataFields(resp.getCustomer());
                } else{
                    showSnack(getContext().getResources().getString(R.string.enter_valid_account_number));
                }
            } else{
                showSnack(getContext().getResources().getString(R.string.enter_valid_account_number));
            }
        });
    }
}