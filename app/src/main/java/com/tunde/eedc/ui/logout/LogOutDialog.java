package com.tunde.eedc.ui.logout;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.tunde.eedc.ui.LoginActivity;
import com.tunde.eedc.ui.MainActivity;
import com.tunde.eedc.R;

public class LogOutDialog extends DialogFragment {


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.dialog_logout, container, false);

        TextView proceedButton = (TextView) root.findViewById(R.id.text_proceed);
        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.mAuth.signOut();

                Intent mainIntent = new Intent(getContext(), LoginActivity.class);
                getActivity().startActivity(mainIntent);
                getActivity().finish();
            }
        });


        TextView cancelButton = (TextView) root.findViewById(R.id.text_cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        return root;
    }

}