package com.tunde.eedc.ui.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.tunde.eedc.services.DataModels.customerModels.Customer;
import com.tunde.eedc.ui.MainActivity;
import com.tunde.eedc.R;
import com.tunde.eedc.utils.PrefManager;

public class ProfileFragment extends Fragment {

    private ProfileViewModel profileViewModel;
    private View root;
    private TextView etName, etEmail, etAccountNumber, etMeterNumber,  etAddress, etDistrict, etTariff;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        profileViewModel =
                ViewModelProviders.of(this).get(ProfileViewModel.class);
        root = inflater.inflate(R.layout.fragment_profile, container, false);
        MainActivity.setHomeIcon(true);


        initViews();

        return root;
    }

    private void initViews() {
        etName = root.findViewById(R.id.text_name);
        etEmail = root.findViewById(R.id.text_email);
        etAccountNumber = root.findViewById(R.id.text_account_number);
        etMeterNumber = root.findViewById(R.id.text_meter_number);
        etAddress = root.findViewById(R.id.text_address);
        etDistrict = root.findViewById(R.id.text_district);
        etTariff = root.findViewById(R.id.text_tariff);
        
        setObservers();
        if(PrefManager.getUserData(getContext()) != null){
            setDataFields(PrefManager.getUserData(getContext()));
        }
    }

    private void setObservers() {
        profileViewModel.getmName().observe(this, s -> etName.setText(s));

        profileViewModel.getmEmail().observe(this, s -> etEmail.setText(s));

        profileViewModel.getmAccountNumber().observe(this, s -> etAccountNumber.setText(s));

        profileViewModel.getmMeterNumber().observe(this, s -> etMeterNumber.setText(s));

        profileViewModel.getmAddress().observe(this, s -> etAddress.setText(s));

        profileViewModel.getmDistrict().observe(this, s -> etDistrict.setText(s));
        
        profileViewModel.getmTerrif().observe(this, s -> etTariff.setText(s));
    }

    private void setDataFields(Customer userData) {
        profileViewModel.setmAccountNumber(userData.getAccountNumber());
        profileViewModel.setmAddress(userData.getAddress());
        profileViewModel.setmName(userData.getFirstName()+" "+userData.getLastName());
        profileViewModel.setmEmail(userData.getEmailAddress());
        profileViewModel.setmMeterNumber(userData.getMeterNumber());
        profileViewModel.setmDistrict(userData.getDistrict());
        profileViewModel.setmTerrif(userData.getTariffCode());
    }

}