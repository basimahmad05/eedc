package com.tunde.eedc.ui.terriffInfo;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.tunde.eedc.R;
import com.tunde.eedc.ui.webView.WebViewActivity;
import com.tunde.eedc.utils.Constants;

public class TerriffInfoFragment extends Fragment {

    private TerriffInfoViewModel terriffInfoViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        terriffInfoViewModel =
                ViewModelProviders.of(this).get(TerriffInfoViewModel.class);
        View root = inflater.inflate(R.layout.fragment_terrif_info, container, false);

        final TextView textView = root.findViewById(R.id.text_share);
        Intent intent = new Intent(getContext(), WebViewActivity.class);
        intent.putExtra(Constants.PrefConstants.webViewIntent,
                "http://enugudisco.com/index.php/extensions/tariff-information/current-tariff");
        getActivity().startActivity(intent);

        terriffInfoViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}