package com.tunde.eedc.ui;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.messaging.FirebaseMessaging;
import com.tunde.eedc.R;
import com.tunde.eedc.utils.Constants;
import com.tunde.eedc.utils.PrefManager;
import com.tunde.eedc.viewModels.ApiViewModel;

import in.codeshuffle.typewriterview.TypeWriterView;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();
    public static MainActivity mainActivity = null;
    private AppBarConfiguration mAppBarConfiguration;
    static MenuItem home = null;
    static TypeWriterView floatingText = null;
    public static NavController navController;
    public static FirebaseAuth mAuth;
    Boolean isUserLogdIn = false;
    ApiViewModel apiViewModel = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        floatingText = findViewById(R.id.text_floating);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        subscribeToNotifications();
        mainActivity = this;
        apiViewModel = ViewModelProviders.of(this).get(ApiViewModel.class);


        mAuth = FirebaseAuth.getInstance();
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home,  R.id.nav_faq, R.id.nav_tariff_info,
                R.id.nav_contact_info, R.id.nav_request_for_meter, R.id.nav_payment_detail,
                R.id.nav_quick_guide, R.id.nav_logout)
                .setDrawerLayout(drawer)
                .build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
//        NavigationUI.setupWithNavController(navigationView, navController);

        getIntentData();

        updateFloatingText();
        final Handler ha=new Handler();
        ha.postDelayed(new Runnable() {

            @Override
            public void run() {
                updateFloatingText();
                ha.postDelayed(this, 5000);
            }
        }, 10000);
    }

    private void subscribeToNotifications() {
        FirebaseMessaging.getInstance().subscribeToTopic(Constants.NotificationTopis.all)
                .addOnCompleteListener(task -> {
                    String msg = "notification subscription completed";
                    if (!task.isSuccessful()) {
                        msg = "notification subscription failed";
                    }
                    Log.d(TAG, msg);
                });
    }

    private void getIntentData() {

        isUserLogdIn = getIntent().getBooleanExtra("IS_LOGGED_IN", false);

        if(isUserLogdIn){
            navController.navigate(R.id.nav_customer_detail);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        home = menu.findItem(R.id.action_home);
        home.setVisible(false);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_home:
                showHomeFragment();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void showHomeFragment(){
        navController.navigate(R.id.nav_home);
    }

    public static void setHomeIcon(Boolean isVisible){
        if(home != null) {
            home.setVisible(isVisible);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this,
                R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void updateFloatingText() {

        if(PrefManager.loadPreferences(this, Constants.PrefConstants.floatingText).equals("0")){
            runOnUiThread(() -> floatingText.setVisibility(View.GONE));

        } else {
            runOnUiThread(() -> {
                floatingText.setVisibility(View.VISIBLE);


                //Setting each character animation delay
                floatingText.setDelay(5000);

                //Setting music effect On/Off
                floatingText.setWithMusic(false);

                //Animating Text
                try {
                    floatingText.animateText(PrefManager.loadPreferences(getApplicationContext(),
                            Constants.PrefConstants.floatingText));
                }catch (Exception e){
                    
                }
            });

        }

    }

}
