package com.tunde.eedc.ui.terriffInfo;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class TerriffInfoViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public TerriffInfoViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Loading Tariff Information...");
    }

    public LiveData<String> getText() {
        return mText;
    }
}