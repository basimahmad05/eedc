package com.tunde.eedc.ui.paymentDetails;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class PaymentDetailModel extends ViewModel {

    public MutableLiveData<String> mAmount;
    public MutableLiveData<String> mEmail;
    public MutableLiveData<String> mPhone;

    public PaymentDetailModel() {
        mAmount = new MutableLiveData<>();
        mEmail = new MutableLiveData<>();
        mPhone = new MutableLiveData<>();
    }

    public MutableLiveData<String> getmAmount() {
        return mAmount;
    }


    public void setmAmount(String data){ mAmount.setValue(data); }


    public MutableLiveData<String> getmEmail() {
        return mEmail;
    }

    public void setmEmail(String data){ mEmail.setValue(data); }


    public MutableLiveData<String> getmPhone() {
        return mPhone;
    }

    public void setmPhone(String data){ mPhone.setValue(data); }

}