package com.tunde.eedc.ui.notifications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.onebyte.trypcustomer.services.models.ErrorModels.CustomNotification;
import com.tunde.eedc.R;
import com.tunde.eedc.services.DataModels.notificationModels.NotificationListAdapter;
import com.tunde.eedc.utils.PrefManager;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

public class NotificationFragment extends Fragment {

    private NotificationViewModel notificationViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationViewModel =
                ViewModelProviders.of(this).get(NotificationViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notification, container, false);

        if(PrefManager.getNotificationList(getContext()) != null &&
                PrefManager.getNotificationList(getContext()).getNotificationList() != null) {

            ArrayList<CustomNotification> list = PrefManager.getNotificationList(getContext()).getNotificationList();
            Collections.reverse(list);
            RecyclerView recyclerView = (RecyclerView) root.findViewById(R.id.rv_notifications);
            NotificationListAdapter adapter = new NotificationListAdapter(list);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(adapter);
        } else {
            Toast.makeText(getContext(),"No notification found",Toast.LENGTH_LONG).show();
        }
        return root;
    }
}