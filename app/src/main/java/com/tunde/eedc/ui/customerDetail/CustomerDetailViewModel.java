package com.tunde.eedc.ui.customerDetail;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.tunde.eedc.services.DataModels.customerModels.UserDataApiResponse;
import com.tunde.eedc.services.Repositories.AppRepository;
import com.tunde.eedc.utils.Validation;

public class CustomerDetailViewModel extends ViewModel {

    public MutableLiveData<String> mAccountNumber;
    public MutableLiveData<String> mAddress;
    public MutableLiveData<String> mBilledDate;
    public MutableLiveData<String> mArrearsBalance;
    public MutableLiveData<String> mBilledAmount;
    public MutableLiveData<String> mVat;

    private LiveData<UserDataApiResponse> userDataObservable = null;


    public CustomerDetailViewModel() {
        mAccountNumber = new MutableLiveData<>();
        mAddress = new MutableLiveData<>();
        mBilledDate = new MutableLiveData<>();
        mArrearsBalance = new MutableLiveData<>();
        mBilledAmount = new MutableLiveData<>();
        mVat = new MutableLiveData<>();
    }


    public void userDetailViewModelCall(Context context, String accountNumber){
        if (Validation.isConnected(context)) {
            userDataObservable = AppRepository.getInstance().getUserDataCall(context, accountNumber);
        }
    }

    public LiveData<UserDataApiResponse> getUserDetail(){
        return userDataObservable;
    }

    public void setAccountNumber(String data){ mAccountNumber.setValue(data); }

    public LiveData<String> getAccountNumber() {
        return mAccountNumber;
    }

    public void setAddress(String data){ mAddress.setValue(data); }

    public LiveData<String> getAddress() {
        return mAddress;
    }

    public void setBilledDate(String data){ mBilledDate.setValue(data); }

    public LiveData<String> getBilledDate() {
        return mBilledDate;
    }

    public void setmArrearsBalance(String data){ mArrearsBalance.setValue(data); }

    public LiveData<String> getArrearBalance() {
        return mArrearsBalance;
    }

    public void setBilledAmount(String data){ mBilledAmount.setValue(data); }

    public LiveData<String> getBilledAmount() {
        return mBilledAmount;
    }

    public void setVat(String data){ mVat.setValue(data); }

    public LiveData<String> getVat() {
        return mVat;
    }


}