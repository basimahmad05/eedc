package com.tunde.eedc.ui;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.tunde.eedc.BuildConfig;
import com.tunde.eedc.R;
import com.tunde.eedc.utils.Constants;
import com.tunde.eedc.utils.PrefManager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashActivity extends AppCompatActivity {


    private static String TAG = "SplashActivity";
    private final int SPLASH_DISPLAY_LENGTH = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        subscribeToNotifications();
        printHashKey(this);
        generateFireBaseToken();
    }

    private void generateFireBaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            String newToken = instanceIdResult.getToken();
            PrefManager.savePreferences(Constants.PrefConstants.deviceToken, newToken,
                    getApplicationContext());
            Log.e("NEW_TOKEN",newToken);
        });
        moveToNextScreen();

    }

    private void subscribeToNotifications() {
        FirebaseMessaging.getInstance().subscribeToTopic(Constants.NotificationTopis.all)
                .addOnCompleteListener(task -> {
                    String msg = "notification subscription completed";
                    if (!task.isSuccessful()) {
                        msg = "notification subscription failed";
                    }
                    Log.d(TAG, msg);
                });
    }

    private void moveToNextScreen() {
        new Handler().postDelayed(() -> {

            Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
            SplashActivity.this.startActivity(mainIntent);
            SplashActivity.this.finish();
        }, SPLASH_DISPLAY_LENGTH);
    }

    public static void printHashKey(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.e(TAG, "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "printHashKey()", e);
        } catch (Exception e) {
            Log.e(TAG, "printHashKey()", e);
        }
    }

}
