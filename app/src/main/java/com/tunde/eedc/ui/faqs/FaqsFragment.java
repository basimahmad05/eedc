package com.tunde.eedc.ui.faqs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.tunde.eedc.R;

public class FaqsFragment extends Fragment {

    private FaqsViewModel faqsViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        faqsViewModel =
                ViewModelProviders.of(this).get(FaqsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_faqs, container, false);

        return root;
    }
}