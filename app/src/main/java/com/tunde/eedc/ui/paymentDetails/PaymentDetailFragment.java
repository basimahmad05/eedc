package com.tunde.eedc.ui.paymentDetails;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.tunde.eedc.R;
import com.tunde.eedc.services.DataModels.customerModels.Customer;
import com.tunde.eedc.utils.PrefManager;

import java.util.Objects;

public class PaymentDetailFragment extends Fragment {

    private PaymentDetailModel paymentDetailModel;
    private View root;
    private EditText etAmount, etEmail, etPhone;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        paymentDetailModel =
                ViewModelProviders.of(this).get(PaymentDetailModel.class);
        root = inflater.inflate(R.layout.fragment_payment_detail, container, false);
        initViews();
        setObservers();
        if(PrefManager.getUserData(getContext()) != null){
            setDataFields(Objects.requireNonNull(PrefManager.getUserData(getContext())));
        }
        return root;
    }

    private void initViews() {
        etAmount = root.findViewById(R.id.text_amount);
        etEmail = root.findViewById(R.id.text_email);
        etPhone = root.findViewById(R.id.text_phone);
    }

    private void setDataFields(Customer userData) {
        paymentDetailModel.setmEmail(userData.getEmailAddress());
        paymentDetailModel.setmPhone(userData.getPhoneNumber());
    }

    private void setObservers() {
        paymentDetailModel.getmAmount().observe(this, s -> etAmount.setText(s));

        paymentDetailModel.getmEmail().observe(this, s -> etEmail.setText(s));

        paymentDetailModel.getmPhone().observe(this, s -> etPhone.setText(s));

    }


}