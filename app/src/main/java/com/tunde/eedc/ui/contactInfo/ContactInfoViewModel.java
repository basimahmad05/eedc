package com.tunde.eedc.ui.contactInfo;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ContactInfoViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ContactInfoViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is slideshow fragment 1");
    }

    public LiveData<String> getText() {
        return mText;
    }
}