package com.tunde.eedc.ui.requestForMeter;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.tunde.eedc.R;
import com.tunde.eedc.ui.webView.WebViewActivity;
import com.tunde.eedc.utils.Constants;

public class RequestForMetersFragment extends Fragment  implements
        AdapterView.OnItemSelectedListener  {

    private RequestForMetersViewModel requestForMetersViewModel;
    String[] spinnerOptions = { "Request for Meter", "Track Status"};

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        requestForMetersViewModel =
                ViewModelProviders.of(this).get(RequestForMetersViewModel.class);
        View root = inflater.inflate(R.layout.fragment_request_for_meters, container, false);

        //Getting the instance of Spinner and applying OnItemSelectedListener on it
        Spinner spin = (Spinner) root.findViewById(R.id.spinner);
        spin.setOnItemSelectedListener(this);

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_item,spinnerOptions);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spin.setAdapter(aa);

        return root;
    }

    //Performing action onItemSelected and onNothing selected
    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        if(position == 0){
            Intent intent = new Intent(getContext(), WebViewActivity.class);
            intent.putExtra(Constants.PrefConstants.webViewIntent,
                    "https://meterrequest.myeedc.com");
            getActivity().startActivity(intent);
        } else {
            Intent intent = new Intent(getContext(), WebViewActivity.class);
            intent.putExtra(Constants.PrefConstants.webViewIntent,
                    "https://meterrequest.myeedc.com/checkstatus.html");
            getActivity().startActivity(intent);
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }
}