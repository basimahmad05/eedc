package com.tunde.eedc.ui.quickGuide;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.tunde.eedc.R;

public class QuickGuideFragment extends Fragment {

    private QuickGuideViewModel quickGuideViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        quickGuideViewModel =
                ViewModelProviders.of(this).get(QuickGuideViewModel.class);
        View root = inflater.inflate(R.layout.fragment_quick_guide, container, false);
        final TextView textView = root.findViewById(R.id.text_share);
        quickGuideViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}