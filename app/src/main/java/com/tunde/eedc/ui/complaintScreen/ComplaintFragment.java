package com.tunde.eedc.ui.complaintScreen;

import android.app.AlertDialog;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;
import com.tunde.eedc.services.DataModels.complaintModels.ComplaintDataModel;
import com.tunde.eedc.services.DataModels.customerModels.Customer;
import com.tunde.eedc.ui.MainActivity;
import com.tunde.eedc.R;
import com.tunde.eedc.utils.PrefManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ComplaintFragment extends Fragment  implements AdapterView.OnItemSelectedListener {

    private ComplaintViewModel complaintViewModel;
    private View root;
    Spinner spinnerCountry, spinnerComplaintType;
    Button buttonAddComplaint;
    EditText etName, etAddress, etPhone, etDistrict, etComplaint, etAccountNumber;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        complaintViewModel =
                ViewModelProviders.of(this).get(ComplaintViewModel.class);
        root = inflater.inflate(R.layout.fragment_complaint, container, false);


        initViews();



        return root;
    }

    private void initViews() {
        MainActivity.setHomeIcon(true);
        buttonAddComplaint = root.findViewById(R.id.btn_confirm);
        etName = root.findViewById(R.id.text_name);
        etAddress = root.findViewById(R.id.text_address);
        etPhone = root.findViewById(R.id.text_phone);
        etDistrict = root.findViewById(R.id.text_district_name);
        etComplaint = root.findViewById(R.id.text_complaint);
        etAccountNumber = root.findViewById(R.id.text_account_number);
        setCountrySpinner();
        setComplaintTypeSpinner();
        setClickListeners();
        setObservers();
        if(PrefManager.getUserData(getContext()) != null){
            setDataFields(PrefManager.getUserData(getContext()));
        } else {
            getAccountNumber();
        }
    }

    private void setObservers() {
        complaintViewModel.getAccountNo().observe(this, s -> etAccountNumber.setText(s));

        complaintViewModel.getAddress().observe(this, s -> etAddress.setText(s));

        complaintViewModel.getComplainantNo().observe(this, s -> etPhone.setText(s));

        complaintViewModel.getmComplaintName().observe(this, s -> etName.setText(s));

        complaintViewModel.getDistrict().observe(this, s -> etDistrict.setText(s));

    }

    private void getAccountNumber() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(Objects.requireNonNull(getContext()).getResources().getString(R.string.enter_account_number));

        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("Next",
                (dialog, which) -> fetchUserDetail(input.getText().toString()));
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

        builder.show();
    }

    private void fetchUserDetail(String accountNumber) {
        complaintViewModel.userDetailViewModelCall(getContext(), accountNumber);
        observableUser();
    }

    private void observableUser() {
        complaintViewModel.getUserDetail().observe(this, resp -> {
            if (resp.getResponseCode() == 200) {
                PrefManager.saveUserData(resp.getCustomer(), getContext());
                setDataFields(resp.getCustomer());
            }
        });
    }

    private void setDataFields(Customer data) {
        complaintViewModel.setmComplaintName(data.getAccountNumber());
        complaintViewModel.setAddress(data.getAddress());
        complaintViewModel.setComplainantNo(data.getPhoneNumber());
        complaintViewModel.setDistrict(data.getDistrict());
        complaintViewModel.setAccountNo(data.getAccountNumber());
    }

    private void setClickListeners() {
        buttonAddComplaint.setOnClickListener(v -> {
            if (etName.getText().toString().equals("") || etAddress.getText().toString().equals("") ||
                    etPhone.getText().toString().equals("") || etDistrict.getText().toString().equals("") ||
                    etComplaint.getText().toString().equals("") || etAccountNumber.getText().toString().equals("")) {
                showSnack(Objects.requireNonNull(
                        getContext()).getResources().getString(R.string.please_provide_complete_data));
            } else {
                addComplaint();
            }
        });
    }

    private void addComplaint() {
        ComplaintDataModel complaint = new ComplaintDataModel();
        complaint.setComplaintType(spinnerComplaintType.getSelectedItem().toString());
        complaint.setComplainantName(etName.getText().toString());
        complaint.setComplainantNo(etPhone.getText().toString());
        complaint.setComplaint(etComplaint.getText().toString());
        complaint.setAccountNo(etAccountNumber.getText().toString());
        complaint.setAddress(etAddress.getText().toString());
        complaint.setDistrict(etDistrict.getText().toString());


        complaintViewModel.addComplaintViewModelCall(getContext(), complaint);
        observableAddComplaint();
    }

    private void observableAddComplaint() {
        complaintViewModel.getComplaintApiResp().observe(this, resp -> {
            if (resp.getSuccess()) {
                showSnack(resp.getMessage());
            }
        });
    }

    private void setCountrySpinner() {

        spinnerComplaintType = (Spinner) root.findViewById(R.id.text_country);
        spinnerComplaintType.setOnItemSelectedListener(this);

        List<String> countries = new ArrayList<String>();
        countries.add("USA");
        countries.add("UK");
        countries.add("UAE");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Objects.requireNonNull(getContext()),
                android.R.layout.simple_spinner_item, countries);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerComplaintType.setAdapter(dataAdapter);
    }

    private void setComplaintTypeSpinner() {

        spinnerComplaintType = (Spinner) root.findViewById(R.id.text_complaint_category);
        spinnerComplaintType.setOnItemSelectedListener(this);

        List<String> categories = new ArrayList<String>();
        categories.add("Supply");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Objects.requireNonNull(getContext()),
                android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerComplaintType.setAdapter(dataAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void showSnack(String message) {
        Snackbar snackbar = Snackbar
                .make(root, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }
}