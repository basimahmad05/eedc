package com.tunde.eedc.ui.confirmationScreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.tunde.eedc.ui.MainActivity;
import com.tunde.eedc.R;

public class ConfirmationFragment extends Fragment  {

    private ConfirmationsViewModel confirmationsViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        confirmationsViewModel =
                ViewModelProviders.of(this).get(ConfirmationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_confirmation_screen, container, false);
        MainActivity.setHomeIcon(true);

        return root;
    }


}