package com.tunde.eedc.ui.requestForMeter;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class RequestForMetersViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public RequestForMetersViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is tools fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}