package com.tunde.eedc.interfaces;


import com.tunde.eedc.services.DataModels.complaintModels.AddComplaintApiResponse;
import com.tunde.eedc.services.DataModels.complaintModels.ComplaintDataModel;
import com.tunde.eedc.services.DataModels.customerModels.UserDataApiResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface NetworkApiInterface {


    @Headers("Content-Type: application/json")
    @GET("api/search")
    Call<UserDataApiResponse> getUserData(@Query("queryString") String accountNumber);

    @Headers("Content-Type: application/json")
    @POST("api/v1/cms/complaint/create?key=Ab3$jk!039")
    Call<AddComplaintApiResponse> addComplaint(@Body ComplaintDataModel data);

}
