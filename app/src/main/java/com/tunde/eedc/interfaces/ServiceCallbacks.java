package com.tunde.eedc.interfaces;

public interface ServiceCallbacks {
    void perform();
}