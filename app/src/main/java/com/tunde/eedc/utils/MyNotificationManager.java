package com.tunde.eedc.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;

import androidx.core.app.NotificationCompat;


import com.tunde.eedc.R;
import com.tunde.eedc.ui.LoginActivity;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;




public class MyNotificationManager {

    public static final String NOTIFICATION_CHANNEL_ID = "4655";
    private Context mCtx;

    public MyNotificationManager(Context mCtx) {
        this.mCtx = mCtx;
    }

    public void showSmallNotification(String title, String message, Intent intent) {
        int NOTIFICATION_ID = 234;

        NotificationManager notificationManager = (NotificationManager)
                mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
        String CHANNEL_ID = "my_channel_01";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {


            CharSequence name = "my_channel";
            String Description = "This is my channel";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mChannel.setShowBadge(false);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder builder;
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
            builder = new NotificationCompat.Builder(mCtx,
                    NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setColor(mCtx.getResources().getColor(R.color.colorAccent))
                    .setContentTitle(title)
                    .setChannelId(CHANNEL_ID)
                    .setAutoCancel(true)
                    .setContentText(message);
        } else {
            builder = new NotificationCompat.Builder(mCtx,
                    NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(title)
                    .setChannelId(CHANNEL_ID)
                    .setAutoCancel(true)
                    .setContentText(message);
        }

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(mCtx);
        if (intent != null) {
            stackBuilder.addParentStack(LoginActivity.class);
            stackBuilder.addNextIntent(intent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            builder.setContentIntent(resultPendingIntent);
        }
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }
    //The method will return Bitmap from an image URL
    private Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}