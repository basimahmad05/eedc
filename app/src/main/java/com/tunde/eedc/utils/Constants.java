package com.tunde.eedc.utils;

public class Constants {
    public static class PrefConstants{
        public static String webViewIntent = "webViewIntent";
        public static String deviceToken = "deviceToken";
        public static String floatingText = "floatingText";
    }

    public static class NotificationTopis{
        public static String all = "all";
    }
}
