package com.tunde.eedc.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.onebyte.trypcustomer.services.models.ErrorModels.CustomNotificationList;
import com.tunde.eedc.services.DataModels.customerModels.Customer;

public class PrefManager {

    private static final String PREF_KEY_USER_DATA = "PREF_KEY_USER_DATA";
    private static final String PREF_KEY_NOTIFICATION_DATA = "PREF_KEY_NOTIFICATION_DATA";


    
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    public static Gson gson = new Gson();

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "eedcPref";


    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        gson = new Gson();
    }





    /**
     * Save date in preferences
     */
    public static void savePreferences(String key, String value, Context context) {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(key, value);
        edit.commit();
    }

    /**
     * Save boolean date in preferences
     */
    public static void saveBooleanPreferences(String key, boolean value, Context context) {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = sp.edit();
        edit.putBoolean(key, value);
        edit.commit();
    }

    /**
     * Load date in preferences
     */
    public static String loadPreferences(Context context, String Key) {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(context);
        String Value = sp.getString(Key, "0");
        return Value;

    }

    /**
     * Load zero preferences
     */
    public static String loadZeroPreferences(Context context, String Key) {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(context);
        String Value = sp.getString(Key, "0");
        return Value;

    }

    /**
     * Load boolean date in preferences
     */
    public static boolean loadBooleanPreferences(Context context, String Key) {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(context);
        boolean Value = sp.getBoolean(Key, false);
        return Value;

    }



    /**
     * Remove specific date from preferences
     */
    public static void removePrefrence(Context context, String key) {
        SharedPreferences myFavePref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = myFavePref.edit();
        editor.remove(key);
        editor.apply();
    }




    /**
     * Save userData data in preferences
     */
    public static void saveUserData(Customer user, Context context) {

        gson = new Gson();
        String posJson = gson.toJson(user);

        savePreferences(PREF_KEY_USER_DATA, posJson, context);
    }

    public static Customer getUserData(Context context) {
        gson = new Gson();
        if (loadPreferences(context, PREF_KEY_USER_DATA).equals("0")){
            return null;
        }else {
            Customer pos = gson.fromJson(loadPreferences(context, PREF_KEY_USER_DATA), Customer.class);
            return pos;
        }
    }

    public static void saveNotificationList(CustomNotificationList data, Context context) {

        gson = new Gson();
        String posJson = gson.toJson(data);

        savePreferences(PREF_KEY_NOTIFICATION_DATA, posJson, context);
    }


    public static CustomNotificationList getNotificationList(Context context) {
        gson = new Gson();
        if (loadPreferences(context, PREF_KEY_NOTIFICATION_DATA).equals("0")){
            return null;
        }else {
            CustomNotificationList pos = gson.fromJson(loadPreferences(context, PREF_KEY_NOTIFICATION_DATA),
                    CustomNotificationList.class);
            return pos;
        }
    }

    /**
     * Remove all data from preferences
     */
    public static void ClearPref(Context context){

    }

}