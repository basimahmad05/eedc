package com.tunde.eedc.services.DataModels.notificationModels;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.onebyte.trypcustomer.services.models.ErrorModels.CustomNotification;
import com.tunde.eedc.R;

import java.util.ArrayList;

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder>{
    private ArrayList<CustomNotification> listdata;

    // RecyclerView recyclerView;
    public NotificationListAdapter(ArrayList<CustomNotification> listdata) {
        this.listdata = listdata;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_notification, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final CustomNotification myListData = listdata.get(position);
        holder.title.setText(myListData.getTitle());
        holder.description.setText(myListData.getDescription()+"\n"+myListData.getDate());

    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView description;

        public ViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.text_title);
            this.description = (TextView) itemView.findViewById(R.id.text_description);
        }
    }
}