package com.tunde.eedc.services.services;

import android.annotation.SuppressLint;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.tunde.eedc.utils.Constants;
import com.tunde.eedc.utils.PrefManager;


@SuppressLint("Registered")
public class MyFirebaseInstanceIDService extends FirebaseMessagingService {


    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        PrefManager.savePreferences(Constants.PrefConstants.deviceToken, token,
                getApplicationContext());
        Log.e("NEW_TOKEN",token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
    }

}
