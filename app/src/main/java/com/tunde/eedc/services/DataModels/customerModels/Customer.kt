package com.tunde.eedc.services.DataModels.customerModels

data class Customer(
    val accountNumber: String = "",
    val arrears: String = "",
    val address: String = "",
    val arrearsBalance: Double = 0.0,
    val billedAmount: Double = 0.0,
    val billedDate: String = "",
    val city: String = "",
    val customerType: String = "",
    val district: String = "",
    val emailAddress: String = "",
    val firstName: String = "",
    val lastName: String = "",
    val lastPayDate: String = "",
    val lastToken: String = "",
    val meterNumber: String = "",
    val paymentPlan: String = "",
    val phoneNumber: String = "",
    val state: String = "",
    val tariffCode: String = "",
    val tariffRate: Double = 0.0,
    val totalDue: Double = 0.0,
    val userCategory: String = "",
    val vat: Double = 0.0,
    val wallet: Double = 0.0
)