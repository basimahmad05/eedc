package com.tunde.eedc.services.DataModels.customerModels

data class UserDataApiResponse(
        val customer: Customer,
        val responseCode: Int,
        val responseMessage: String
)