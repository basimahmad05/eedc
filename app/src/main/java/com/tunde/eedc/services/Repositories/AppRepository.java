package com.tunde.eedc.services.Repositories;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.onebyte.trypcustomer.services.models.ErrorModels.StatusModel;
import com.tunde.eedc.interfaces.NetworkApiInterface;
import com.tunde.eedc.services.DataModels.complaintModels.AddComplaintApiResponse;
import com.tunde.eedc.services.DataModels.complaintModels.ComplaintDataModel;
import com.tunde.eedc.services.DataModels.customerModels.UserDataApiResponse;
import com.tunde.eedc.services.Network.CustomRequest;
import com.tunde.eedc.services.Network.CustomRequestPort;
import com.tunde.eedc.utils.ProgressHUD;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class AppRepository {
    private NetworkApiInterface networkApiInterface, networkApiInterfacePort;
    private NetworkApiInterface intercomNetworkApiInterface;
    private static AppRepository repository;
    private ProgressHUD progress;

    private AppRepository() {


        Retrofit retrofit = CustomRequest.getClient();
        Retrofit retrofitPort = CustomRequestPort.getClient();

        networkApiInterface = retrofit.create(NetworkApiInterface.class);
        networkApiInterfacePort = retrofitPort.create(NetworkApiInterface.class);
    }

    public synchronized static AppRepository getInstance() {
        if (repository == null) {
            if (repository == null) {
                repository = new AppRepository();
            }
        }
        return repository;
    }



    public LiveData<UserDataApiResponse> getUserDataCall(Context context, String accountNumber) {
        showpDialog(context, "Please Wait", "");
        final MutableLiveData<UserDataApiResponse> data = new MutableLiveData<>();
        networkApiInterface.getUserData(accountNumber).enqueue(new Callback<UserDataApiResponse>() {
            @Override
            public void onResponse(Call<UserDataApiResponse> call, Response<UserDataApiResponse> response) {
                hidepDialog();
                if (response != null) {
                    if (response.isSuccessful()) {
                        data.setValue(response.body());
                    } else {
                        try {
                            Gson gson = new Gson();
                            StatusModel errorModel = gson.fromJson(response.errorBody().charStream(),
                                    StatusModel.class);
                            Toast.makeText(context, errorModel.getStatus().getMessage(), Toast.LENGTH_LONG)
                                    .show();
                        } catch (Exception e) {
                            Log.e("sendPhoneEmailError", "" + e);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<UserDataApiResponse> call, Throwable t) {
                // TODO better error handling in part #2 ...
                hidepDialog();
                data.setValue(null);
            }
        });

        return data;
    }

    public LiveData<AddComplaintApiResponse> addComplaintCall(Context context, ComplaintDataModel dataObj) {
        showpDialog(context, "Please Wait", "");
        final MutableLiveData<AddComplaintApiResponse> data = new MutableLiveData<>();
        networkApiInterfacePort.addComplaint(dataObj).enqueue(new Callback<AddComplaintApiResponse>() {
            @Override
            public void onResponse(Call<AddComplaintApiResponse> call, Response<AddComplaintApiResponse> response) {
                hidepDialog();
                if (response != null) {
                    if (response.isSuccessful()) {
                        data.setValue(response.body());
                    } else {
                        try {
                            Gson gson = new Gson();
                            StatusModel errorModel = gson.fromJson(response.errorBody().charStream(),
                                    StatusModel.class);
                            Toast.makeText(context, errorModel.getStatus().getMessage(), Toast.LENGTH_LONG)
                                    .show();
                        } catch (Exception e) {
                            Log.e("sendPhoneEmailError", "" + e);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<AddComplaintApiResponse> call, Throwable t) {
                // TODO better error handling in part #2 ...
                hidepDialog();
                data.setValue(null);
            }
        });

        return data;
    }


    private void showpDialog(Context context, String message, String description) {
        progress = ProgressHUD.showLoadingDialog(context, message, description);
    }

    private void hidepDialog() throws IllegalArgumentException {
        if (progress.isShowing()) {
            progress.dismiss();
        }
    }
}
