package com.onebyte.trypcustomer.services.models.ErrorModels

import com.google.gson.annotations.SerializedName



data class RootError(
        @SerializedName("code")
        var code: Int,
        @SerializedName("description")
        var description: String,
        @SerializedName("reasonPhrase")
        var reasonPhrase: String
)