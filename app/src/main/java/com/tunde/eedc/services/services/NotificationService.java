package com.tunde.eedc.services.services;

import android.annotation.SuppressLint;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.onebyte.trypcustomer.services.models.ErrorModels.CustomNotification;
import com.onebyte.trypcustomer.services.models.ErrorModels.CustomNotificationList;
import com.tunde.eedc.ui.MainActivity;
import com.tunde.eedc.utils.Constants;
import com.tunde.eedc.utils.MyNotificationManager;
import com.tunde.eedc.utils.PrefManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@SuppressLint("Registered")
public class NotificationService extends FirebaseMessagingService {

    private String TAG = NotificationService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getNotification() != null) {
            if (remoteMessage.getNotification().getTitle().equals("1101")) {
                PrefManager.savePreferences(Constants.PrefConstants.floatingText,
                        remoteMessage.getNotification().getBody(), getApplicationContext());
                if (MainActivity.mainActivity != null) {
                    MainActivity.mainActivity.updateFloatingText();
                }

            } else {
                showNotification(remoteMessage.getNotification().getTitle(),
                        remoteMessage.getNotification().getBody());
            }
        }
    }

    private void showNotification(String title, String message) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();


        CustomNotification notificationObj = new CustomNotification();
        notificationObj.setTitle(title);
        notificationObj.setDescription(message);
        notificationObj.setDate(formatter.format(date));
        CustomNotificationList notificationList = PrefManager.getNotificationList(getApplicationContext());
        if (notificationList == null) {

            notificationList = new CustomNotificationList();
            ArrayList<CustomNotification> list = new ArrayList<>();
            list.add(notificationObj);

            notificationList.setNotificationList(list);

        } else {
            if (notificationList.getNotificationList() == null) {
                ArrayList<CustomNotification> list = new ArrayList<>();
                list.add(notificationObj);

                notificationList.setNotificationList(list);

            } else {
                notificationList.getNotificationList().add(notificationObj);
            }
        }

        PrefManager.saveNotificationList(notificationList, getApplicationContext());
        MyNotificationManager mNotificationManager = new MyNotificationManager(getApplicationContext());
        mNotificationManager.showSmallNotification(title, message, null);

    }
}
