package com.onebyte.trypcustomer.services.models.ErrorModels

import com.google.gson.annotations.SerializedName



data class CustomNotificationList(
        @SerializedName("notificationList")
        var notificationList: ArrayList<CustomNotification>? = null
)