package com.onebyte.trypcustomer.services.models.ErrorModels

import com.google.gson.annotations.SerializedName
import com.tunde.eedc.services.DataModels.Status


data class StatusModel(
        @SerializedName("status")
        var status: Status? = null
)