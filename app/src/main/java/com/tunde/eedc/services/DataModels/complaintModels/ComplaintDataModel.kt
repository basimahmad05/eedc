package com.tunde.eedc.services.DataModels.complaintModels

data class ComplaintDataModel(
    var accountNo: String = "",
    var address: String = "",
    var complainantName: String = "",
    var complainantNo: String = "",
    var complaint: String = "",
    var complaintType: String = "",
    var district: String = "",
    var status: String = "",
    var proiority: String = "",
    var smsSent: String = "",
    var _id: String = "",
    var ticketNo: String = "",
    var timeStamp: String = ""
)