package com.onebyte.trypcustomer.services.models.ErrorModels

import com.google.gson.annotations.SerializedName



data class CustomNotification(
        @SerializedName("title")
        var title: String = "",
        @SerializedName("description")
        var description: String = "",
        @SerializedName("date")
        var date: String = ""
)