package com.tunde.eedc.services.DataModels.complaintModels

data class AddComplaintApiResponse(
        val customer: ComplaintDataModel,
        val success: Boolean,
        val message: String
)